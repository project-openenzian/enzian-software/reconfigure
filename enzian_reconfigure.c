/*---------------------------------------------------------------------------*/
// Copyright (c) 2024 ETH Zurich.
// All rights reserved.
//
// This file is distributed under the terms in the attached LICENSE file.
// If you do not find this file, copies can be found by writing to:
// ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
/*---------------------------------------------------------------------------*/

// Enzian Reconfigure
// Reconfigure the dynamic region of the FPGA using the AXI HWICAP IP
// HWICAP is exposed in the FPGA I/O space at 0x97ef'ffff'f000
// Accepts .bit or .bin files

#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>

// HWICAP registers
#define HWICAP_BASE 0x97EFFFFFF000UL
#define HWICAP_GIER 0x0e
#define HWICAP_ISR  0x10
#define HWICAP_IER  0x14
#define HWICAP_WF   0x80
#define HWICAP_RF   0x82
#define HWICAP_SZ   0x84
#define HWICAP_CR   0x86
#define HWICAP_SR   0x88
#define HWICAP_WFV  0x8a
#define HWICAP_RFO  0x8c
#define HWICAP_ASR  0x8e

// Shell status/control register, 64-bit
#define SHELL_STATUS 0x3fc

// Shell git version
#define SHELL_VERSION 0x3fe

volatile uint32_t *hwicap;

int reconfigure(unsigned char *bitstream, uint32_t size)
{
    uint32_t sr, cr, wf, wfv;
    unsigned i, j;

    sr = hwicap[HWICAP_SR];
    if (sr != 5) {
        printf("SR: %x\nEOS and DONE not set!\n", sr);
        exit(1);
    }
    cr = hwicap[HWICAP_CR];
    if (cr != 0) {
        printf("CR: %x is not zero!\n", sr);
        exit(1);
    }
    for (i = 0; i < size;) {
        wfv = hwicap[HWICAP_WFV]; // Fill the FIFO
        printf("%d/%d\r", i, size);
        for (j = 0; j < 4 * wfv; j += 4) {
            if (i + j >= size)
                break;
            wf = htonl(*(uint32_t *)(bitstream + i + j));
            hwicap[HWICAP_WF] = wf;
        }
        hwicap[HWICAP_CR] = 1; // Empty the FIFO
        for (;;) {
            cr = hwicap[HWICAP_CR];
            if ((cr & 1) == 0)
                break;
        }
        i += j;
    }
    printf("%d/%d\n", i, size);
    return 0;
}

// Skip the header and find the actual bitstream, look for bus width detect words, 0x000000bb, 0x11220044
// Return the size of the actual bitstream
uint32_t find_bin(unsigned char *bitstream, uint32_t size)
{
    uint32_t i;

    for (i = 0; i < 256; i++) { // find the bus width header
        if (bitstream[i] == 0x00 && bitstream[i + 1] == 0x00 && bitstream[i + 2] == 0x00 && bitstream[i + 3] == 0xbb &&
            bitstream[i + 4] == 0x11 && bitstream[i + 5] == 0x22 && bitstream[i + 6] == 0x00 && bitstream[i + 7] == 0x44) {
            printf("Header found at %d\n", i);
            memmove(bitstream, bitstream + i, size - i); // skip to it
            return size - i;
        }
    }
    printf("Header not found!\n");
    exit(1);
}

int main(int argc, char *argv[])
{
    int fd = 0;
    unsigned char *bitstream;
    ssize_t bitstream_size;

    if (argc == 1) {
        printf("Reconfigure the Enzian FPGA\n");
        printf("Requiers access to /dev/mem, may need root privilages\n");
        printf("Usage: %s [.bit or .bin bitstream file]\n", argv[0]);
        return 0;
    }

    bitstream = malloc(128 * 1024 * 1024); // uncompressed bitstream is about 78M
    assert(bitstream);

    fd = open("/dev/mem", O_RDWR);
    assert(fd >= 0);

    // Map the FPGA I/O space
    hwicap = mmap(NULL, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, fd, HWICAP_BASE);
    assert(hwicap != MAP_FAILED);
    close(fd);

    printf("Enzian Shell version: %08x\n", hwicap[SHELL_VERSION]);

    fd = open(argv[1], O_RDONLY);
    bitstream_size = read(fd, bitstream, 128 * 1024 * 1024);
    assert(bitstream_size > 0 && bitstream_size < 128 * 1024 * 1024);
    close(fd);

    printf("Bitstream file size: %ld\n", bitstream_size);

    bitstream_size = find_bin(bitstream, bitstream_size);
    printf("Binstream size: %ld\n", bitstream_size);

    // Decouple the FPGA app port from the Shell
    printf("Decoupling...\n");
    hwicap[SHELL_STATUS] |= 1;

    reconfigure(bitstream, bitstream_size);

    // Couple the FPGA app port
    printf("Coupling...\n");
    hwicap[SHELL_STATUS] &= ~1;

    // Reset the FPGA app
    printf("Resetting the app...\n");
    hwicap[SHELL_STATUS] |= 2;
    hwicap[SHELL_STATUS] &= ~2;

    printf("Done!\n");

    return 0;
}
